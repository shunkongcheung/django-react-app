from rest_framework.generics import GenericAPIView
from rest_framework.mixins import UpdateModelMixin, RetrieveModelMixin
from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField, CharField
from rest_framework.metadata import SimpleMetadata
from django.utils.encoding import force_text

from uam.models import Function, FunctionGroup

class FunctionUpdateSerializer(ModelSerializer):
    function_groups = PrimaryKeyRelatedField(many=True, queryset=FunctionGroup.objects.all())
    # password = CharField(style={'input_type': 'password'})
    class Meta:
        model = Function
        fields = ['name', 'view_url', 'function_groups']

class APIRootMetadata(SimpleMetadata):
    def get_serializer_info(self, serializer):
        for field_name, field in serializer.fields.items():
            if self.label_lookup[field] == 'field':
                field.child_relation.queryset = field.child_relation.queryset
        return super(APIRootMetadata, self).get_serializer_info(serializer)

    def get_field_info(self, field):
        field_info = super(APIRootMetadata, self).get_field_info(field)

        if field.style.get('input_type', None) is not None:
            field_info['input_type'] = field.style['input_type']
        if self.label_lookup[field] == 'field':    
            field_info['choices'] = [
                {
                    'value': choice_value,
                    'display_name': force_text(choice_name, strings_only=True)
                }
                for choice_value, choice_name in field.choices.items()
            ]
        return field_info

class FunctionObjectAPIView(UpdateModelMixin, RetrieveModelMixin, GenericAPIView):
    queryset = Function.objects.all()
    serializer_class = FunctionUpdateSerializer
    metadata_class = APIRootMetadata

    def post(self, request, *args, **kwargs):
        return self.update(request, args, kwargs)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, args, kwargs)
    