from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    url(r'^function/(?P<pk>[0-9]+)/$', views.FunctionObjectAPIView.as_view(), name='function'),
]

