# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

class Function(models.Model):
    TYPE_USER = 'USER'
    TYPE_AUTO = 'AUTO'
    TYPE_TIME = 'TIME'
    TYPE_MSG = 'MSG'
    TYPE_CHOICES = [(TYPE_MSG,'User'),(TYPE_AUTO,'Auto'),(TYPE_TIME,'Time'),(TYPE_MSG,'Msg')]

    name = models.CharField(max_length=50)
    fun_type = models.CharField(max_length=4, choices=TYPE_CHOICES)

    # external usage. For calling the api
    view_url = models.URLField()
    create_url = models.URLField(blank=True, default='')
    edit_url = models.URLField(blank=True, default='')
    delete_url = models.URLField(blank=True, default='')
    internal_path = models.CharField(max_length=100, blank=True, default='')

    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, related_name="+", on_delete=models.CASCADE)
    modified_at = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(User, related_name="+", on_delete=models.CASCADE)
    enable = models.BooleanField(default=True)
    def __str__(self):
        return self.name

class FunctionGroup(models.Model):
    name = models.CharField(max_length=50)
    functions = models.ManyToManyField(Function, related_name="function_groups", blank=True)
    
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, related_name="+", on_delete=models.CASCADE)
    modified_at = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(User, related_name="+", on_delete=models.CASCADE)
    enable = models.BooleanField(default=True)
    def __str__(self):
        return self.name