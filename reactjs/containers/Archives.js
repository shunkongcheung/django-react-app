import React from "react";
import { connect } from "react-redux"

import { getFunctionFormOption } from "../actions/functionAction"

import Form from "../components/Form"

@connect(state => ({
  myfunction: state.function,
}))
export default class Archives extends React.Component {
  componentWillMount(){
    let {dispatch} = this.props;
    alert(dispatch);
    dispatch(getFunctionFormOption())
  }
  render() {
    let {myfunction} = this.props;
    const inputs = {
      "first_name": {
          "type": "string",
          "required": false,
          "read_only": false,
          "label": "First name",
          "max_length": 30
      },
      "last_name": {
          "type": "string",
          "required": false,
          "read_only": false,
          "label": "Last name",
          "max_length": 30
      }
    };
    return (
      <div>
        <h1>Archives</h1>
        <Form method="post" formProps={myfunction.formProps} />
      </div>
    );
  }
}