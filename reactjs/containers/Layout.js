import React from "react";

import { connect } from "react-redux";
import { Link } from "react-router";

import Footer from "../components/Layout/Footer";
import TopNav from "../components/Layout/TopNav";
import Navigation from "../components/Layout/Navigation";

// import * as counterActions from "../actions/counterAction"

@connect(state => ({
  counters: state.counters,
}))
export default class SampleAppContainer extends React.Component {
  // handleClick() {
  //   let { dispatch } = this.props;
  //   dispatch(counterActions.increaseCounter())
  // }
  render() {
    // let { counters } = this.props
    const style={
      bodyStyle:{
        height:'calc(100vh - 250px)'
      }
    }
    return (
      <div>
        <TopNav location={location} />
        <div className="mai-wrapper">
          <Navigation />
          <div className="py-2 px-6 mx-6" style={style.bodyStyle}>
            {this.props.children}
          </div>
          <Footer />

        </div>

      </div>
    )
  }
}