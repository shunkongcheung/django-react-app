import React from "react";
import { render } from "react-dom";
import ReactDOM from "react-dom";
import {
  createStore,
  compose,
  applyMiddleware,
  combineReducers,
} from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { BrowserRouter as Router, Route } from 'react-router-dom';

import * as reducers from "./reducers"
import Archives from "./containers/Archives";
import Layout from "./containers/Layout";

let finalCreateStore = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore)

let reducer = combineReducers(reducers)
let store = finalCreateStore(reducer)

ReactDOM.render(
  // <div>heyhey</div>
  <Provider store={store}>
    <Layout>
      <Router>
        <div>
        <Route path="/" component={Archives} />
        </div>
      </Router>
    </Layout>
  </Provider>,
  document.getElementById('root')
);