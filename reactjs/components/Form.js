import React from "react";

export default class Form extends React.Component {
    render() {
        const formProps = this.props.formProps;
        var keys = [];
        Object.keys(formProps).forEach(function (key) { keys.push(key) })
        return (
            <div className="bg-white border p-3 h-100">
            <form className="form-horizontal h-100" action="" method={this.props.method} id="std-form" encType="multipart/form-data">
                {keys.map((key) =>
                    <div className="form-group d-flex">
                        <div className="col-3 d-flex align-items-center">{formProps[key].label}</div>
                        <div className="col-7">
                            <input className='form-control' />
                        </div>
                    </div>
                )}

            </form>
            </div>
        )
    }
}