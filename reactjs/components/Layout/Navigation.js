import React from "react";

export default class Navigation extends React.Component {
    render() {
        const functions = [
            { 'url': 'url1', 'name': 'function1' },
            { 'url': 'url2', 'name': 'function2' }
        ]
        var mappedFunctions = functions.map((fun) =>
            <li className="nav-item">
                <a href={fun.url} className={"nav-link " + function_active}>
                    <span className="icon"></span>
                    <span className="name">{fun.name}</span>
                </a>
            </li>
        )
        const fun_group_open = location.pathname.match(/^/) ? "open" : "";
        const function_active = location.pathname.match(/^/) ? "active" : "";
        return (
            <nav className="navbar navbar-expand-lg mai-sub-header bg-grey">
                <div className="container">
                    <nav className="navbar navbar-expand-md ">
                        <button type="button" data-toggle="collapse" data-target="#mai-navbar-collapse" aria-controls="#mai-navbar-collapse" aria-expanded="false"
                            aria-label="Toggle navigation" className="navbar-toggler hidden-md-up collapsed">
                            <div className="icon-bar">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </button>
                        <div id="mai-navbar-collapse" className="navbar-collapse collapse mai-nav-tabs">
                            <ul className="nav navbar-nav">
                                <li className={"nav-item parent " + fun_group_open}>
                                    <a href="#" role="button" aria-expanded="false" className="nav-link">
                                        <span className="icon"></span>
                                        <span>Group</span>
                                    </a>
                                    <ul className="mai-nav-tabs-sub mai-sub-nav nav">
                                        {mappedFunctions}
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div >
            </nav >
        )
    }
}