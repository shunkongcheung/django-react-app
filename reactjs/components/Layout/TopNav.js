import React from "react";
import { IndexLink, Link } from "react-router";


export default class Navigation extends React.Component {
  constructor() {
    super()
    this.state = {
      collapsed: true,
    };
  }

  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  render() {
    const { location } = this.props;
    const { collapsed } = this.state;
    const featuredClass = location.pathname === "/" ? "active" : "";
    const archivesClass = location.pathname.match(/^\/archives/) ? "active" : "";
    const settingsClass = location.pathname.match(/^\/settings/) ? "active" : "";
    const navClass = collapsed ? "collapse" : "";
    const style={
      navbarHeight:{
        height:'50px'
      },
      logoHeight:{
        height:'45px'
      }
    }
    return (
      <nav className="navbar navbar-expand mai-top-header" >
         <div className="container h-100">
            <div className="mx-5 px-5 d-flex w-100 h-100">
                <a href="#" className="navbar-brand h-100 d-flex">
                    <img src={"/static/base/images/logo_white%402x.png"} style={style.logoHeight} />
                </a>
                <ul className="nav navbar-nav mai-top-nav"></ul>
                <ul className="navbar-nav float-lg-right mai-icons-nav">
                    <li className="dropdown nav-item mai-notifications">
                        <a to="#" data-toggle="dropdown" role="button" aria-expanded="false" className="nav-link dropdown-toggle">
                            <span className="icon s7-bell"></span>
                            <span className="indicator"></span>
                        </a>
                        <ul className="dropdown-menu">
                            <li>
                                <div className="title">Notifications</div>
                            </li>
                        </ul>
                    </li>
                    <li className="dropdown nav-item mai-settings">
                        <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" className="nav-link dropdown-toggle">
                            <span className="icon s7-settings"></span>
                        </a>
                        <ul className="dropdown-menu">
                            <li>
                                <div className="title">Settings</div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul className="nav navbar-nav float-lg-right mai-user-nav">
                    <li className="dropdown nav-item">
                        <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" className="dropdown-toggle nav-link">
                            <img src={'/static/base/assets/img/avatar.jpg'} />
                            <span className="user-name">Admin</span>
                            <span className="angle-down s7-angle-down"></span>
                        </a>
                        <div role="menu" className="dropdown-menu">
                            <a href="#" className="dropdown-item ">
                                <span className="icon s7-user"></span>Profile</a>
                            <a href="#" className="dropdown-item border-bottom">
                                <span className="icon s7-tools"></span>Reset Password</a>
                            <a href="#" className="dropdown-item">
                                <span className="icon s7-power"> </span>Log Out</a>
                        </div>
                    </li>
                </ul> 
            </div>
        </div> 
      </nav>
    );
  }
}