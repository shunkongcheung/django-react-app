import React from "react";

export default class Footer extends React.Component {
  
  render() {
    const style={
      footer:{
        fontSize:'10px',
        minHeight:'40px'
      }
    }
    return (
      <div className="fixed-bottom bg-dark text-white d-flex justify-content-center align-items-center" style={style.footer}>
        © Copyright 2018 Radica System Limited. All Rights Reserved.
      </div>

    );
  }
}